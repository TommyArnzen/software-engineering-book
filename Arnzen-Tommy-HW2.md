Name: Tommy Arnzen<br>
Due Date: Monday, September 18th, 2017<br>
Assignment: Software Engineering Book<br>
Class: Software Engineering<br>

#Aspects of Software Engineering

<b>I.	Introduction – What is software engineering?</b><br>
&nbsp;&nbsp;&nbsp;a.	“A branch of computer science that deals with design, implementation, and maintenance of complex software systems.” (Marriam-Websters Dictionary)<br>
&nbsp;&nbsp;&nbsp;b.	The four factors of software engineering.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i.	Human, design, technology, and process.<br><br>


<b>II.	The product life cycle.</b><br>
&nbsp;&nbsp;&nbsp;a.	Evolution, teamwork, manage objects, measurement and evaluation, and documentation.<br>
&nbsp;&nbsp;&nbsp;b.	Requirements, architecture and design, construction, and deployment and maintenance.<br><br>


<b>III.	Characteristics of Software Development.</b><br>
&nbsp;&nbsp;&nbsp;a.	Essential Characteristics.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i.	Complexity, conformity, changeability, invisible, implicit, and evolution.<br>
&nbsp;&nbsp;&nbsp;b.	Accidental Characteristics.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i.	Inadequate modes/means of expression.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii.	Inadequate abstractions.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iii.	Inadequate support.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv.	Limited resources.<br><br>


<b>IV.	Brooks Law – The Mythical Man-Month.</b><br>
&nbsp;&nbsp;&nbsp;a.	Adding manpower to a long software project makes it longer.
<br><br>


<b>V.	The Silver Bullet Law</b><br>
&nbsp;&nbsp;&nbsp;a.	“No single development, in either technology or management technique, can by itself promise even an order of magnitude in improvement, productivity, reliability, or simplicity.”
<br><br>

<b>VI.	Software Engineering Management.</b><br>
&nbsp;&nbsp;&nbsp;a.	Client requests, changing technology, and changing conditions.<br>
&nbsp;&nbsp;&nbsp;b.	Three levels of project management.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i.	Organizational and Infrastructure.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii.	Project management.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iii.	Measurement program management.<br>
<br><br>


<b>VII.	Conclusion.</b><br>
<br><br>

<h3 style="text-align:center;">Bibliography</h3><br>

1.	“Software Engineering.” Merriam-Webster, Merriam-Webster, www.merriam-webster.com/dictionary/software%20engineering.

